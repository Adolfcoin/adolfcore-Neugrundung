### The Neugrunding fork has reached its last Neugrundung cycle. This repository exists for historical purposes.

# Quick Start
[Pool Mining](https://fuhrerbunker.adolfcoin.camp/c/tech-help/fluffballs-mining-pool)  
[Running a wallet and node on Windows](https://github.com/Adolfcoin/adolfcore-Neugrundung/releases)  
[User guide for wallet and node](https://github.com/Adolfcoin/Wiki/wiki)

It's important to be involved in the community and not simply mine coins. Adolfcoin is in a constant state of change and being disconnected from the community means you will miss out on important updates, faster miners, forking options, etc. [The Fuhrerbunker](https://fuhrerbunker.adolfcoin.camp) is the main center for the community (and also the best place to get help).

# Compiling Adolfcoin from Source

Getting help: if you run into a problem compiling please log as much detail as possible to the [issue tracker](https://github.com/Adolfcoin/adolfcore-Neugrundung/issues).

## Overview:

Adolfcoin Dependencies: GCC 4.7.3 or later, CMake 2.8.6 or later, libunbound 1.4.16 or later (note: Unbound is not a dependency, libunbound is), libevent 2.0 or later, libgtest 1.5 or later, and Boost 1.55 or later, BerkeleyDB 4.8 or later.
Static Build Additional Dependencies: ldns 1.6.17 or later, expat 1.1 or later, bison or yacc.

**Basic Process:**

* Install the dependencies
* Get the source code
* Run `make` in the root directory.
* The resulting executables can be found in `build/release/bin` or `build/debug/bin`, depending on what you're building.

**More options:**

* Build faster: run `make -j<number of threads>` instead of `make`. Use `nproc` first to see how many cores you have.
* Debug build: run `make debug`.
* Test suite: run `make release-test` to run tests in addition to building. Running `make debug-test` will do the same to the debug version.

**Static Build Targets:**

For static builds there are a number of Makefile targets.

* ```make release-static-win64``` builds statically for 64-bit Windows systems
* ```make release-static-win32``` builds statically for 32-bit Windows systems
* ```make release-static-64``` builds statically for Linux or Mac
* ```make release-static-32``` builds statically for Linux or Mac (32 bit)
* ```make release-static-arm6``` builds statically for ARMv6 devices, such as the Raspberry Pi

## Ubuntu

The following works on a fresh install of Ubuntu 16.04 LTS  
#### Install Dependencies
```sudo apt-get update  && sudo apt-get -y upgrade  && sudo apt-get -y  dist-upgrade && sudo apt-get install -y build-essential cmake pkg-config libboost-all-dev libevent-dev libunbound-dev libminiupnpc-dev libdb{,++}-dev doxygen graphviz libldns-dev libexpat1-dev libunwind8-dev git```  

#### Clone the source  

```git clone  https://github.com/Adolfcoin/adolfcore-Neugrundung.git```  

#### Compile
```cd adolfcore-Neugrundung```  
```make``` 

#### Run
Upon completion, the binaries will be found in build/release/bin/
```cd build/release/bin/```  
```./adolfnode```

If you run into problems, ask here on Github or on the [forum](https://fuhrerbunker.adolfcoin.camp/c/tech-help).

## MacOS X:

#### Install Xcode
Click on the search icon in the top-right of your desktop, and search for `terminal`. Open terminal.   
Type ```xcode-select --install``` into the terminal.
You should now see a popup - click `Install` and follow the instructions.

#### Install [Homebrew](https://brew.sh):

```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```

#### Install the dependencies:

`brew doctor`
Once this completes, you should see `Your system is ready to brew`.

`brew update` to update the database of available packages.

`brew install libevent berkeley-db boost openssl git` to install the dependencies.

#### Clone the source  

Type `pwd` to see which folder you are currently in. Wherever you are, a new folder called 'adolfcore-Neugrundung' will be created in the following step and this will contain the Adolfcoin program later.

```git clone  https://github.com/Adolfcoin/adolfcore-Neugrundung.git```  

#### Set up environment variables:

```export OPENSSL_INCLUDE_DIR=/usr/local/Cellar/openssl/1.0.2d_1/include/```  
```export OPENSSL_ROOT_DIR=/usr/local/opt/openssl/ ```

#### Compile a statically linked 64-bit release:

```make release-static-64```

#### Run
Upon completion, the binaries will be found in build/release/bin/  
```cd build/release/bin/```  
```./adolfnode```

You can also navigate to the folder using finder and simply doulble click on `adolfnode`.

To start mining, see the [Wiki](https://github.com/Adolfcoin/Wiki/wiki)

## On Windows:

Adolfcoin tries to support mentally challenged computers, at least whenever possible.

Dependencies: mingw-w64, msys2, CMake 2.8.6 or later, libunbound 1.4.16 or later (note: Unbound is not a dependency, libunbound is), and Boost 1.55 or later, BerkeleyDB 4.8 or later.

**Preparing the Build Environment**

* Download the [MSYS2 installer](http://msys2.github.io), 64-bit or 32-bit as needed, and run it.
* Use the shortcut associated with your architecture to launch the MSYS2 environment. On 64-bit systems that would be the MinGW-w64 Win64 Shell shortcut. Note that if you are running 64-bit Windows, you will have both 64-bit and 32-bit environments.
* Update the packages in your MSYS2 install:
```
pacman -Sy
pacman -Su --ignoregroup base
pacman -Su
```
* Install dependencies: `pacman -S mingw-w64-x86_64-gcc make mingw-w64-x86_64-cmake mingw-w64-x86_64-unbound mingw-w64-x86_64-boost`
* If you are planning to build statically you will also need to install: `pacman -S mingw-w64-x86_64-ldns mingw-w64-x86_64-expat` (note that these are likely already installed by the unbound dependency installation above)

**Building**

* From the root of the source code directory run:
```
mkdir build
cd build
```
* If you are on a 64-bit system, run:
```
cmake -G "MSYS Makefiles" -D CMAKE_BUILD_TYPE=Release -D CMAKE_C_FLAGS="-Wa,-mbig-obj -O3" -D CMAKE_CXX_FLAGS="-Wa,-mbig-obj -O3" -D ARCH="x86-64" -D BUILD_64=ON -D CMAKE_TOOLCHAIN_FILE=../cmake/64-bit-toolchain.cmake -D MSYS2_FOLDER=c:/msys64 ..
```
* If you are on a 32-bit system, run:
```
cmake -G "MSYS Makefiles" -D CMAKE_BUILD_TYPE=Release -D CMAKE_C_FLAGS="-Wa,-mbig-obj -O3" -D CMAKE_CXX_FLAGS="-Wa,-mbig-obj -O3" -D ARCH="i686" -D BUILD_64=OFF -D CMAKE_TOOLCHAIN_FILE=../cmake/32-bit-toolchain.cmake -D MSYS2_FOLDER=c:/msys32 ..
```
* You can now run `make` to have it build
* The resulting executables can be found in `build/release/bin` or `build/debug/bin`, depending on what you're building.

If you installed MSYS2 in a folder other than c:/msys64, make the appropriate substitution above.

**More options:**

* Parallel build: run `make -j<number of threads>` instead of `make`.
* Statically linked release build: run `make release-static-win64` or `make release-static-win32`.
* Debug build: run `make debug`.
* Test suite: run `make release-test` to run tests in addition to building. Running `make debug-test` will do the same to the debug version.

## Building Documentation

Adolfcoin developer documentation uses Doxygen, and is currently a work-in-progress.

Dependencies: Doxygen 1.8.0 or later, Graphviz 2.28 or later (optional).

* To build, change to the root of the source code directory, and run `doxygen Doxyfile`
* If you have installed Graphviz, you can also generate in-doc diagrams by instead running `HAVE_DOT=YES doxygen Doxyfile`
* The output will be built in doc/html/

## Contributing
Please follow the [Führerprocess protocol](https://github.com/Adolfcoin/Wiki/wiki) when contributing to the project. The Führerprocess is in essence a hill-climbing algorithm, or in the words of our chief scientist Willy Heidinger: "a drunken stumble to greatness".

The block chain concept provides peer-to-peer and decentralised electronic cash. It was originally believed that by trading development flexibility for mass consus - making future modifications to the consensus rules difficult without near unanimous agreement - human problems that would arise during the development process would work themselves out. This trade-off has resulted in an iron dome over the development process, moving from stalemate to stalemate and creating an environment amenable to the worst aspects of humanity.

The Führerprocess and indeed Adolfcoin itself is an experiment in addressing these concerns. As such, no one is exempt from the rules of the Führerprocess. This is to ensure that Adolfcoin is protected from the influence of charismatic leaders, even if their name is Barry Silbert, Donald Trump, or The Führer. 

You are of course very welcome to contribute to Adolfcoin, and there are in fact many obvious improvements that could be made. The Führer, yourself, and anyone else in the world have equal rights when it comes to having pull requests merged. You do not need to seek any kind of permission or consensus about the project direction before submitting a pull request, and no one (not even the Fuhrer) can reject your pull request if it follows the Fuhrerprocess protocol. Please take half an hour to [read and understand the Führerprocess](https://github.com/Adolfcoin/Wiki/wiki) and the rationale behind it.

Please also do your best to follow the [style guide](https://github.com/Adolfcoin/Wiki/wiki) when contributing.

Along with your first pull request, please include your name (or pseudonym) in the AUTHORS file.

## Ownership and License
Adolfcoin is Copyright(c) the _contributors_ as listed in the AUTHORS file. Adolfcoin uses the MPL v2 license, see the LICENSE file. Contributors to Adolfcoin maintain copyright to their code and share ownership of the Adolfcoin project.

### Why MPL v2
The MPL v2 ensures that contributors' code may not be used in any project which does not release their own improvements under a share-alike license and thus allowing these improvements to be easily merged back into the contributor's own project (whether that be Adolfcoin or their own fork). 

The Führer would never ask anyone to contribute to a BSD/MIT/etc licensed project where the contributor's work may be relicensed without their permission. Under this style of license, your work can be taken by a corporate enterprise and locked up under their own license preventing you from adopting any of their improvements (which, by the way, ends up competing with the project you contributed towards). The MPL v2 is far more egalitarian and in harmony with the spirit of open source software - anyone who uses your code must make their own improvements available to you.
