################################################################################
# 	This file is part of the Adolfcoin project and is Copyright © The		   #
# 	Contributors listed in the AUTHORS file at the project root. This project  #
# 	is released under the MPL v2 license contained in the LICENSE file at the  #
# 	root of this project.                                                      #
#                                                                              #
# 	Any derived works must be released under the MPL v2 (or later) license.    #
# 																			   #
# 	Parts of this project are originally Copyright © The Monero Project, The   #
# 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash  #
# 	Developers. For full details see the LEGACY_LICENSES file at the root of   #
# 	this project.                                                              #
################################################################################

MESSAGE(STATUS "Looking for libunbound")

FIND_PATH(UNBOUND_INCLUDE_DIR
  NAMES unbound.h
  PATH_SUFFIXES include/ include/unbound/
  PATHS "${PROJECT_SOURCE_DIR}"
  ${UNBOUND_ROOT}
  $ENV{UNBOUND_ROOT}
  /usr/local/
  /usr/
)

find_library(UNBOUND_LIBRARIES unbound)
