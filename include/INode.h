////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <cstdint>
#include <system_error>

namespace CryptoNote {

class INodeObserver {
public:
  virtual void initCompleted(std::error_code result) {}

  virtual void peerCountUpdated(size_t count) {}
  virtual void lastLocalBlockHeightUpdated(uint64_t height) {}
  virtual void lastKnownBlockHeightUpdated(uint64_t height) {}

  virtual void blockchainReorganized(uint64_t height) {}
};

class INode {
public:
  virtual ~INode() = 0;
  virtual void addObserver(INodeObserver* observer) = 0;
  virtual void removeObserver(INodeObserver* observer) = 0;

  virtual void init() = 0;
  virtual void shutdown() = 0;

  virtual size_t getPeerCount() = 0;
  virtual uint64_t getLastLocalBlockHeight() = 0;
  virtual uint64_t getLastKnownBlockHeight() = 0;
};

}
