////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "crypto/crypto.h"
#include "adolfcoin_core/adolfcoin_basic.h"

#include "single_tx_test_base.h"

class test_generate_key_derivation : public single_tx_test_base
{
public:
  static const size_t loop_count = 1000;

  bool test()
  {
    crypto::key_derivation recv_derivation;
    crypto::generate_key_derivation(m_tx_pub_key, m_bob.get_keys().m_view_secret_key, recv_derivation);
    return true;
  }
};
