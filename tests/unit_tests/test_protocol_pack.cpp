////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"

#include "include_base_utils.h"
#include "protocol_handler/protocol_definitions.h"
#include "storages/portable_storage_template_helper.h"

TEST(protocol_pack, protocol_pack_command) 
{
  std::string buff;
  cryptonote::NOTIFY_RESPONSE_CHAIN_ENTRY::request r;
  r.start_height = 1;
  r.total_height = 3;
  for(int i = 1; i < 10000; i += i*10)
  {
    r.m_block_ids.resize(i, boost::value_initialized<crypto::hash>());
    bool res = epee::serialization::store_t_to_binary(r, buff);
    ASSERT_TRUE(res);

    cryptonote::NOTIFY_RESPONSE_CHAIN_ENTRY::request r2;
    res = epee::serialization::load_t_from_binary(r2, buff);
    ASSERT_TRUE(r.m_block_ids.size() == i);
    ASSERT_TRUE(r.start_height == 1);
    ASSERT_TRUE(r.total_height == 3);
  }
}
