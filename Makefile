################################################################################
# 	This file is part of the Adolfcoin project and is Copyright © The		   #
# 	Contributors listed in the AUTHORS file at the project root. This project  #
# 	is released under the MPL v2 license contained in the LICENSE file at the  #
# 	root of this project.                                                      #
#                                                                              #
# 	Any derived works must be released under the MPL v2 (or later) license.    #
# 																			   #
# 	Parts of this project are originally Copyright © The Monero Project, The   #
# 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash  #
# 	Developers. For full details see the LEGACY_LICENSES file at the root of   #
# 	this project.                                                              #
################################################################################

all: basic

basic-debug:
	mkdir -p build/release
	cd build/release && cmake  -D CMAKE_CXX_FLAGS="-Wno-narrowing" -D BUILD_TESTS=OFF -D CMAKE_BUILD_TYPE=Debug ../.. && $(MAKE)


cmake-debug:
	mkdir -p build/debug
	cd build/debug && cmake -D CMAKE_BUILD_TYPE=Debug ../..

debug: cmake-debug
	cd build/debug && $(MAKE)

debug-test: debug
	mkdir -p build/debug
	cd build/debug && cmake -D BUILD_TESTS=ON -D CMAKE_BUILD_TYPE=Debug ../.. && $(MAKE) test

debug-all:
	mkdir -p build/debug
	cd build/debug && cmake -D BUILD_TESTS=ON -D CMAKE_BUILD_TYPE=Debug ../.. && $(MAKE)

cmake-release:
	mkdir -p build/release
	cd build/release && cmake -D CMAKE_BUILD_TYPE=Release ../..

release: cmake-release
	cd build/release && $(MAKE)

release-test: release
	mkdir -p build/release
	cd build/release && cmake -D BUILD_TESTS=ON -D CMAKE_BUILD_TYPE=release ../.. && $(MAKE) test

release-all:
	mkdir -p build/release
	cd build/release && cmake -D BUILD_TESTS=ON -D CMAKE_BUILD_TYPE=release ../.. && $(MAKE)
	
basic:
	mkdir -p build/release
	cd build/release && cmake  -D CMAKE_CXX_FLAGS="-Wno-narrowing" -D BUILD_TESTS=OFF -D CMAKE_BUILD_TYPE=release ../.. && $(MAKE)

release-static: release-static-64

release-static-64:
	mkdir -p build/release
	cd build/release && cmake  -D CMAKE_CXX_FLAGS="-Wno-narrowing" -D STATIC=ON -D ARCH="x86-64" -D BUILD_64=ON -D CMAKE_BUILD_TYPE=release ../.. && $(MAKE)

release-static-32:
	mkdir -p build/release
	cd build/release && cmake  -D CMAKE_CXX_FLAGS="-Wno-narrowing" -D STATIC=ON -D ARCH="i686" -D BUILD_64=OFF -D CMAKE_BUILD_TYPE=release ../.. && $(MAKE)

release-static-arm6:
	mkdir -p build/release
	cd build/release && cmake -D STATIC=ON -D ARCH="armv6zk" -D BUILD_64=OFF -D NO_AES=ON -D CMAKE_BUILD_TYPE=release ../.. && $(MAKE)

release-static-win64:
	mkdir -p build/release
	cd build/release && cmake  -D CMAKE_CXX_FLAGS="-Wno-narrowing" -G "MSYS Makefiles" -D STATIC=ON -D ARCH="x86-64" -D BUILD_64=ON -D CMAKE_BUILD_TYPE=Release -D CMAKE_TOOLCHAIN_FILE=../../cmake/64-bit-toolchain.cmake -D MSYS2_FOLDER=c:/msys64 ../.. && $(MAKE)

release-static-win32:
	mkdir -p build/release
	cd build/release && cmake -G "MSYS Makefiles" -D STATIC=ON -D ARCH="i686" -D BUILD_64=OFF -D CMAKE_BUILD_TYPE=Release -D CMAKE_TOOLCHAIN_FILE=../../cmake/32-bit-toolchain.cmake -D MSYS2_FOLDER=c:/msys32 ../.. && $(MAKE)

clean:
	@echo "WARNING: Back-up your wallet if it exists within ./build!" ; \
        read -r -p "This will destroy the build directory, continue (y/N)?: " CONTINUE; \
	[ $$CONTINUE = "y" ] || [ $$CONTINUE = "Y" ] || (echo "Exiting."; exit 1;)
	rm -rf build

tags:
	ctags -R --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ src contrib tests/gtest

.PHONY: all cmake-debug debug debug-test debug-all cmake-release release release-test release-all clean tags
