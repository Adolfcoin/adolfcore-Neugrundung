////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <boost/filesystem.hpp>
#include "adolfcoin_core/blockchain.h" // BlockchainDB
#include "adolfcoin_core/blockchain_storage.h" // in-memory DB
#include "blockchain_db/blockchain_db.h"
#include "blockchain_db/lmdb/db_lmdb.h"

using namespace cryptonote;


#if !defined(BLOCKCHAIN_DB) || BLOCKCHAIN_DB == DB_LMDB

struct fake_core_lmdb
{
  Blockchain m_storage;
  tx_memory_pool m_pool;
  bool support_batch;
  bool support_add_block;

  // for multi_db_runtime:
#if !defined(BLOCKCHAIN_DB)
  fake_core_lmdb(const boost::filesystem::path &path, const bool use_testnet=false, const bool do_batch=true, const int mdb_flags=0) : m_pool(&m_storage), m_storage(m_pool)
  // for multi_db_compile:
#else
  fake_core_lmdb(const boost::filesystem::path &path, const bool use_testnet=false, const bool do_batch=true, const int mdb_flags=0) : m_pool(m_storage), m_storage(m_pool)
#endif
  {
    m_pool.init(path.string());

    BlockchainDB* db = new BlockchainLMDB();

    boost::filesystem::path folder(path);

    folder /= db->get_db_name();

    LOG_PRINT_L0("Loading blockchain from folder " << folder.string() << " ...");

    const std::string filename = folder.string();
    try
    {
      db->open(filename, mdb_flags);
    }
    catch (const std::exception& e)
    {
      LOG_PRINT_L0("Error opening database: " << e.what());
      throw;
    }

    m_storage.init(db, use_testnet);
    if (do_batch)
      m_storage.get_db().set_batch_transactions(do_batch);
    support_batch = true;
    support_add_block = true;
  }
  ~fake_core_lmdb()
  {
    m_storage.deinit();
  }

  uint64_t add_block(const block& blk
                            , const size_t& block_size
                            , const difficulty_type& cumulative_difficulty
                            , const uint64_t& coins_generated
                            , const std::vector<transaction>& txs
                            )
  {
    return m_storage.get_db().add_block(blk, block_size, cumulative_difficulty, coins_generated, txs);
  }

  void batch_start()
  {
    m_storage.get_db().batch_start();
  }

  void batch_stop()
  {
    m_storage.get_db().batch_stop();
  }

};
#endif

#if !defined(BLOCKCHAIN_DB) || BLOCKCHAIN_DB == DB_MEMORY

struct fake_core_memory
{
  blockchain_storage m_storage;
  tx_memory_pool m_pool;
  bool support_batch;
  bool support_add_block;

  // for multi_db_runtime:
#if !defined(BLOCKCHAIN_DB)
  fake_core_memory(const boost::filesystem::path &path, const bool use_testnet=false) : m_pool(&m_storage), m_storage(m_pool)
#else
  // for multi_db_compile:
  fake_core_memory(const boost::filesystem::path &path, const bool use_testnet=false) : m_pool(m_storage), m_storage(&m_pool)
#endif
  {
    m_pool.init(path.string());
    m_storage.init(path.string(), use_testnet);
    support_batch = false;
    support_add_block = false;
  }
  ~fake_core_memory()
  {
    LOG_PRINT_L3("fake_core_memory() destructor called - want to see it ripple down");
    m_storage.deinit();
  }

  uint64_t add_block(const block& blk
                            , const size_t& block_size
                            , const difficulty_type& cumulative_difficulty
                            , const uint64_t& coins_generated
                            , const std::vector<transaction>& txs
                            )
  {
    // TODO:
    // would need to refactor handle_block_to_main_chain() to have a direct add_block() method like Blockchain class
    throw std::runtime_error("direct add_block() method not implemented for in-memory db");
    return 2;
  }

  void batch_start()
  {
    LOG_PRINT_L0("WARNING: [batch_start] opt_batch set, but this database doesn't support/need transactions - ignoring");
  }

  void batch_stop()
  {
    LOG_PRINT_L0("WARNING: [batch_stop] opt_batch set, but this database doesn't support/need transactions - ignoring");
  }

};

#endif
