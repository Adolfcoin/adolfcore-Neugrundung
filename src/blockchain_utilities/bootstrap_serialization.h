////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "adolfcoin_core/boost_serialization.h"
#include "adolfcoin_core/difficulty.h"


namespace cryptonote
{
  namespace bootstrap
  {

    struct file_info
    {
      uint8_t  major_version;
      uint8_t  minor_version;
      uint32_t header_size;

      BEGIN_SERIALIZE_OBJECT()
        FIELD(major_version);
        FIELD(minor_version);
        VARINT_FIELD(header_size);
      END_SERIALIZE()
    };

    struct blocks_info
    {
      // block heights of file's first and last blocks, zero-based indexes
      uint64_t block_first;
      uint64_t block_last;

      // file position, for directly reading last block
      uint64_t block_last_pos;

      BEGIN_SERIALIZE_OBJECT()
        VARINT_FIELD(block_first);
        VARINT_FIELD(block_last);
        VARINT_FIELD(block_last_pos);
      END_SERIALIZE()
    };

    struct block_package
    {
      cryptonote::block block;
      std::vector<transaction> txs;
      size_t block_size;
      difficulty_type cumulative_difficulty;
      uint64_t coins_generated;

      BEGIN_SERIALIZE()
        FIELD(block)
        FIELD(txs)
        VARINT_FIELD(block_size)
        VARINT_FIELD(cumulative_difficulty)
        VARINT_FIELD(coins_generated)
      END_SERIALIZE()
    };

  }

}
