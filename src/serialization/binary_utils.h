////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <sstream>
#include "binary_archive.h"

namespace serialization {
  /*! creates a new archive with the passed blob and serializes it into v
   */
  template <class T>
    bool parse_binary(const std::string &blob, T &v)
    {
      std::istringstream istr(blob);
      binary_archive<false> iar(istr);
      return ::serialization::serialize(iar, v);
    }

  /*! dumps the data in v into the blob string
   */
  template<class T>
    bool dump_binary(T& v, std::string& blob)
    {
      std::stringstream ostr;
      binary_archive<true> oar(ostr);
      bool success = ::serialization::serialize(oar, v);
      blob = ostr.str();
      return success && ostr.good();
    };

}
