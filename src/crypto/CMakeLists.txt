################################################################################
# 	This file is part of the Adolfcoin project and is Copyright © The		   #
# 	Contributors listed in the AUTHORS file at the project root. This project  #
# 	is released under the MPL v2 license contained in the LICENSE file at the  #
# 	root of this project.                                                      #
#                                                                              #
# 	Any derived works must be released under the MPL v2 (or later) license.    #
# 																			   #
# 	Parts of this project are originally Copyright © The Monero Project, The   #
# 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash  #
# 	Developers. For full details see the LEGACY_LICENSES file at the root of   #
# 	this project.                                                              #
################################################################################

set(crypto_sources
  aesb.c
  blake256.c
  chacha8.c
  crypto-ops-data.c
  crypto-ops.c
  crypto.cpp
  groestl.c
  hash-extra-blake.c
  hash-extra-groestl.c
  hash-extra-jh.c
  hash-extra-skein.c
  hash.c
  jh.c
  keccak.c
  oaes_lib.c
  random.c
  skein.c
  slow-hash.c
  tree-hash.c)

set(crypto_headers)

set(crypto_private_headers
  blake256.h
  chacha8.h
  crypto-ops.h
  crypto.h
  generic-ops.h
  groestl.h
  groestl_tables.h
  hash-ops.h
  hash.h
  initializer.h
  jh.h
  keccak.h
  oaes_config.h
  oaes_lib.h
  random.h
  skein.h
  skein_port.h)

adolfs_private_headers(crypto
  ${crypto_private_headers})
adolfcoin_add_library(crypto
  ${crypto_sources}
  ${crypto_headers}
  ${crypto_private_headers})
