////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stddef.h>
#include <string.h>

#include "hash-ops.h"

#ifndef __FreeBSD__
 #include <alloca.h>
#else
 #include <stdlib.h>
#endif

/// Quick check if this is power of two (use on unsigned types; in this case for size_t only)
bool ispowerof2_size_t(size_t x) {
	return x && !(x & (x - 1));
}

/*** 
* Round to power of two, for count>=3 and for count being not too large (as reasonable for tree hash calculations)
*/
size_t tree_hash_cnt(size_t count) {
	assert( count >= 3); // cases for 0,1,2 are handled elsewhere
	// Round down the count size: fun(2**n)= 2**(n-1) to round down to power of two
	size_t tmp = count - 1;
	size_t jj = 1;
	for (jj=1 ; tmp != 0 ; ++jj) {
		tmp /= 2; // dividing by 2 until to get how many powers of 2 fits size_to tmp
	}
	size_t cnt = 1 << (jj-2); // cnt is the count, but rounded down to power of two
	// printf("count=%zu cnt=%zu jj=%zu tmp=%zu \n" , count,cnt,jj,tmp);
	assert( cnt > 0 );	assert( cnt >= count/2 ); 	assert( cnt <= count );
	assert( ispowerof2_size_t( cnt ));
	return cnt;
}

void tree_hash(const char (*hashes)[HASH_SIZE], size_t count, char *root_hash) {
// Problem: the value of cnt was calculated incorrectly on the Monero blockchain at height 202612 
// (http://moneroblocks.info/search/bbd604d2ba11ba27935e006ed39c9bfdd99b76bf4a50654bc1e1e61217962698) 
// and Adolfcoin has the same vulnerability. This block contained 514 transactions which triggered bad 
// calculation of variable "cnt" in the original version of this function.
//
// The following code removes the error in the algorithm which allowed the exploit to take place on Monero's chain.

  assert(count > 0);
  if (count == 1) {
    memcpy(root_hash, hashes, HASH_SIZE);
  } else if (count == 2) {
    cn_fast_hash(hashes, 2 * HASH_SIZE, root_hash);
  } else {
    size_t i, j;

    size_t cnt = tree_hash_cnt( count );
    size_t max_size_t = (size_t) -1; // max allowed value of size_t 
    assert( cnt < max_size_t/2 ); // reasonable size to avoid any overflows. /2 is extra; Anyway should be limited much stronger by logical code 
    // as we have sane limits on transactions counts in blockchain rules

    char (*ints)[HASH_SIZE];
    size_t ints_size = cnt * HASH_SIZE;
    ints = alloca(ints_size); 	memset( ints , 0 , ints_size);  // allocate, and zero out as extra protection for using uninitialized mem

    memcpy(ints, hashes, (2 * cnt - count) * HASH_SIZE);

    for (i = 2 * cnt - count, j = 2 * cnt - count; j < cnt; i += 2, ++j) {
      cn_fast_hash(hashes[i], 64, ints[j]);
    }
    assert(i == count);

    while (cnt > 2) {
      cnt >>= 1;
      for (i = 0, j = 0; j < cnt; i += 2, ++j) {
        cn_fast_hash(ints[i], 64, ints[j]);
      }
    }

    cn_fast_hash(ints[0], 64, root_hash);
  }
}
