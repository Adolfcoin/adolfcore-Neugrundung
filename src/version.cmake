################################################################################
# 	This file is part of the Adolfcoin project and is Copyright © The		   #
# 	Contributors listed in the AUTHORS file at the project root. This project  #
# 	is released under the MPL v2 license contained in the LICENSE file at the  #
# 	root of this project.                                                      #
#                                                                              #
# 	Any derived works must be released under the MPL v2 (or later) license.    #
# 																			   #
# 	Parts of this project are originally Copyright © The Monero Project, The   #
# 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash  #
# 	Developers. For full details see the LEGACY_LICENSES file at the root of   #
# 	this project.                                                              #
################################################################################

# Check what commit we're on
execute_process(COMMAND "${GIT}" rev-parse --short HEAD RESULT_VARIABLE RET OUTPUT_VARIABLE COMMIT OUTPUT_STRIP_TRAILING_WHITESPACE)

if(RET)
	# Something went wrong, set the version tag to -unknown
	
    message(WARNING "Cannot determine current commit. Make sure that you are building either from a Git working tree or from a source archive.")
    set(VERSIONTAG "unknown")
    configure_file("src/version.h.in" "${TO}")
else()
	message(STATUS "You are currently on commit ${COMMIT}")
	
	# Get all the tags
	execute_process(COMMAND "${GIT}" show-ref --tags -d --abbrev RESULT_VARIABLE RET OUTPUT_VARIABLE TAGGEDCOMMITOUT OUTPUT_STRIP_TRAILING_WHITESPACE)
	
	# Make sure we actually got some tags
	if(TAGGEDCOMMITOUT)
		string(LENGTH ${TAGGEDCOMMITOUT} TLEN)
	else()
		set(TLEN 1)
	endif()
	
	if(RET OR TLEN LESS 5)
	    message(WARNING "Cannot determine most recent tag. Make sure that you are building either from a Git working tree or from a source archive.")
	    set(VERSIONTAG "${COMMIT}")		
	else()
		# Replace a bunch of things so we end up with a semi-colon separated list
		string(REPLACE " refs/" "\n" TAGGEDCOMMITOUT2 ${TAGGEDCOMMITOUT})
		string(REPLACE "\n" ";" TAGGEDCOMMITLIST ${TAGGEDCOMMITOUT2})
		
		# Grab the second-last item in the list, as that will be the hash of our most recent commit
		list(GET TAGGEDCOMMITLIST -2 TAGGEDCOMMIT)

		if(NOT TAGGEDCOMMIT)
			message(WARNING "Cannot determine most recent tag. Make sure that you are building either from a Git working tree or from a source archive.")
			set(VERSIONTAG "${COMMIT}")
		else()
			message(STATUS "The most recent tag was at ${TAGGEDCOMMIT}")
			
			# Check if we're building that tagged commit or a different one
			if(COMMIT STREQUAL TAGGEDCOMMIT)
				message(STATUS "You are building a tagged release")
				set(VERSIONTAG "release")
			else()
				message(STATUS "You are ahead or behind of a tagged release")
				set(VERSIONTAG "${COMMIT}")
			endif()
		endif()	    

	endif()

    configure_file("src/version.h.in" "${TO}")
endif()
