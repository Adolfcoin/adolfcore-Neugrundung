////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>

namespace tools
{

// RFC defines for record types and classes for DNS, gleaned from ldns source
const static int DNS_CLASS_IN  = 1;
const static int DNS_TYPE_A    = 1;
const static int DNS_TYPE_TXT  = 16;
const static int DNS_TYPE_AAAA = 8;

struct DNSResolverData;

/**
 * @brief Provides high-level access to DNS resolution
 *
 * This class is designed to provide a high-level abstraction to DNS resolution
 * functionality, including access to TXT records and such.  It will also
 * handle DNSSEC validation of the results.
 */
class DNSResolver
{
public:

  /**
   * @brief Constructs an instance of DNSResolver
   *
   * Constructs a class instance and does setup stuff for the backend resolver.
   */
  DNSResolver();

  /**
   * @brief takes care of freeing C pointers and such
   */
  ~DNSResolver();

  /**
   * @brief gets ipv4 addresses from DNS query of a URL
   *
   * returns a vector of all IPv4 "A" records for given URL.
   * If no "A" records found, returns an empty vector.
   *
   * @param url A string containing a URL to query for
   *
   * @param dnssec_available 
   *
   * @return vector of strings containing ipv4 addresses
   */
  std::vector<std::string> get_ipv4(const std::string& url, bool& dnssec_available, bool& dnssec_valid);

  /**
   * @brief gets ipv6 addresses from DNS query
   *
   * returns a vector of all IPv6 "A" records for given URL.
   * If no "A" records found, returns an empty vector.
   *
   * @param url A string containing a URL to query for
   *
   * @return vector of strings containing ipv6 addresses
   */
   std::vector<std::string> get_ipv6(const std::string& url, bool& dnssec_available, bool& dnssec_valid);

  /**
   * @brief gets all TXT records from a DNS query for the supplied URL;
   * if no TXT record present returns an empty vector.
   *
   * @param url A string containing a URL to query for
   *
   * @return A vector of strings containing a TXT record; or an empty vector
   */
  // TODO: modify this to accomodate DNSSEC
   std::vector<std::string> get_txt_record(const std::string& url, bool& dnssec_available, bool& dnssec_valid);

  /**
   * @brief Gets a DNS address from OpenAlias format
   *
   * If the address looks good, but contains one @ symbol, replace that with a .
   * e.g. trump@whitehouse.gov becomes trump.whitehouse.gov
   *
   * @param oa_addr  OpenAlias address
   *
   * @return dns_addr  DNS address
   */
  std::string get_dns_format_from_oa_address(const std::string& oa_addr);

  /**
   * @brief Gets the singleton instance of DNSResolver
   *
   * @return returns a pointer to the singleton
   */
  static DNSResolver& instance();

private:

  /**
   * @brief Checks a string to see if it looks like a URL
   *
   * @param addr the string to be checked
   *
   * @return true if it looks enough like a URL, false if not
   */
  bool check_address_syntax(const std::string& addr);

  DNSResolverData *m_data;
}; // class DNSResolver

}  // namespace tools
