////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once 

#include <mutex>
#include <system_error>
#include <boost/filesystem.hpp>

#include "crypto/crypto.h"
#include "crypto/hash.h"
#include "misc_language.h"
#include "p2p/p2p_protocol_defs.h"

/*! \brief Various Tools
 *
 *  
 * 
 */
namespace tools
{
  /*! \brief Returns the default data directory.
   *
   * \details Windows < Vista: C:\\Documents and Settings\\Username\\Application Data\\SHORT_REICHNAME
   *
   * Windows >= Vista: C:\\Users\\Username\\AppData\\Roaming\\SHORT_REICHNAME
   *
   * Mac: ~/Library/Application Support/SHORT_REICHNAME
   *
   * Unix: ~/.SHORT_REICHNAME
   */
  std::string get_default_data_dir();

#ifdef WIN32
  /**
   * @brief 
   *
   * @param nfolder
   * @param iscreate
   *
   * @return 
   */
  std::string get_special_folder_path(int nfolder, bool iscreate);
#endif

  /*! \brief Returns the OS version string
   *
   * \details This is a wrapper around the primitives
   * get_windows_version_display_string() and
   * get_nix_version_display_string()
   */
  std::string get_os_version_string();

  /*! \brief creates directories for a path
   *
   *  wrapper around boost::filesyste::create_directories.  
   *  (ensure-directory-exists): greenspun's tenth rule in action!
   */
  bool create_directories_if_necessary(const std::string& path);
  /*! \brief std::rename wrapper for nix and something strange for windows.
   */
  std::error_code replace_file(const std::string& replacement_name, const std::string& replaced_name);

  inline crypto::hash get_proof_of_trust_hash(const nodetool::proof_of_trust& pot)
  {
    std::string s;
    s.append(reinterpret_cast<const char*>(&pot.peer_id), sizeof(pot.peer_id));
    s.append(reinterpret_cast<const char*>(&pot.time), sizeof(pot.time));
    return crypto::cn_fast_hash(s.data(), s.size());
  }

  /*! \brief Defines a signal handler for win32 and *nix
   */
  class signal_handler
  {
  public:
    /*! \brief installs a signal handler  */
    template<typename T>
    static bool install(T t)
    {
#if defined(WIN32)
      bool r = TRUE == ::SetConsoleCtrlHandler(&win_handler, TRUE);
      if (r)
      {
        m_handler = t;
      }
      return r;
#else
      /* Only blocks SIGINT and SIGTERM */
      signal(SIGINT, posix_handler);
      signal(SIGTERM, posix_handler);
      m_handler = t;
      return true;
#endif
    }

  private:
#if defined(WIN32)
    /*! \brief Handler for win */
    static BOOL WINAPI win_handler(DWORD type)
    {
      if (CTRL_C_EVENT == type || CTRL_BREAK_EVENT == type)
      {
        handle_signal();
      }
      else
      {
        LOG_PRINT_RED_L0("Got control signal " << type << ". Exiting without saving...");
        return FALSE;
      }
      return TRUE;
    }
#else
    /*! \brief handler for NIX */
    static void posix_handler(int /*type*/)
    {
      handle_signal();
    }
#endif

    /*! \brief calles m_handler */
    static void handle_signal()
    {
      static std::mutex m_mutex;
      std::unique_lock<std::mutex> lock(m_mutex);
      m_handler();
    }

    /*! \brief where the installed handler is stored */
    static std::function<void(void)> m_handler;
  };
}
