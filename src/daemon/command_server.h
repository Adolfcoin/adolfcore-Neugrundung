/**
@file
@details


Passing RPC commands:

@image html images/other/runtime-commands.png

*/

////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "console_handler.h"
#include "daemon/command_parser_executor.h"

namespace daemonize {

class t_command_server {
private:
  t_command_parser_executor m_parser;
  epee::console_handlers_binder m_command_lookup;
  bool m_is_rpc;

public:
  t_command_server(
      uint32_t ip
    , uint16_t port
    , bool is_rpc = true
    , cryptonote::core_rpc_server* rpc_server = NULL
    );

  bool process_command_str(const std::string& cmd);

  bool process_command_vec(const std::vector<std::string>& cmd);

  bool start_handling();

  void stop_handling();

private:
  bool help(const std::vector<std::string>& args);

  std::string get_commands_str();
};

} // namespace daemonize
