////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <boost/program_options.hpp>

namespace daemonize {

class t_internals;

class t_daemon final {
public:
  static void init_options(boost::program_options::options_description & option_spec);
private:
  std::unique_ptr<t_internals> mp_internals;
public:
  t_daemon(
      boost::program_options::variables_map const & vm
    );
  t_daemon(t_daemon && other);
  t_daemon & operator=(t_daemon && other);
  ~t_daemon();

  bool run(bool interactive = false);
  void stop();
};
}
