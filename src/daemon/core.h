////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "adolfcoin_core/checkpoints_create.h"
#include "adolfcoin_core/adolfcoin_core.h"
#include "protocol_handler/protocol_handler.h"
#include "misc_log_ex.h"
#include <stdexcept>
#include <boost/program_options.hpp>
#include "daemon/command_line_args.h"

namespace daemonize
{

class t_core final
{
public:
  static void init_options(boost::program_options::options_description & option_spec)
  {
    cryptonote::core::init_options(option_spec);
    cryptonote::miner::init_options(option_spec);
  }
private:
  typedef cryptonote::t_cryptonote_protocol_handler<cryptonote::core> t_protocol_raw;
  cryptonote::core m_core;
  // TEMPORARY HACK - Yes, this creates a copy, but otherwise the original
  // variable map could go out of scope before the run method is called
  boost::program_options::variables_map const m_vm_HACK;
public:
  t_core(
      boost::program_options::variables_map const & vm
    )
    : m_core{nullptr}
    , m_vm_HACK{vm}
  {
  }

  // TODO - get rid of circular dependencies in internals
  void set_protocol(t_protocol_raw & protocol)
  {
    m_core.set_cryptonote_protocol(&protocol);
  }

  void run()
  {
    //initialize core here
    LOG_PRINT_L0("Initializing core...");
    if (!m_core.init(m_vm_HACK))
    {
      throw std::runtime_error("Failed to initialize core");
    }
    LOG_PRINT_L0("Core initialized OK");
  }

  cryptonote::core & get()
  {
    return m_core;
  }

  ~t_core()
  {
    LOG_PRINT_L0("Deinitializing core...");
    try {
      m_core.deinit();
      m_core.set_cryptonote_protocol(nullptr);
    } catch (...) {
      LOG_PRINT_L0("Failed to deinitialize core...");
    }
  }
};

}
