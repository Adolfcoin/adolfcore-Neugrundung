////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma  once


#define WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR           -1
#define WALLET_RPC_ERROR_CODE_WRONG_ADDRESS           -2
#define WALLET_RPC_ERROR_CODE_DAEMON_IS_BUSY          -3
#define WALLET_RPC_ERROR_CODE_GENERIC_TRANSFER_ERROR  -4
#define WALLET_RPC_ERROR_CODE_WRONG_PAYMENT_ID        -5
#define WALLET_RPC_ERROR_CODE_TRANSFER_TYPE           -6
#define WALLET_RPC_ERROR_CODE_DENIED                  -7
