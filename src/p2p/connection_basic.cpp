/// @file
/// @author rfree
/// @brief base for connection, contains e.g. the ratelimit hooks

////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

/* rfree: implementation for the non-template base, can be used by connection<> template class in abstract_tcp_server2 file  */

#include "connection_basic.hpp"

#include <boost/asio.hpp>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <atomic>

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/interprocess/detail/atomic.hpp>
#include <boost/thread/thread.hpp>

#include <memory>

#include "syncobj.h"

#include "../../contrib/epee/include/net/net_utils_base.h" 
#include "../../contrib/epee/include/misc_log_ex.h" 
#include <boost/lambda/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/chrono.hpp>
#include <boost/utility/value_init.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include "misc_language.h"
#include "pragma_comp_defs.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <mutex>

#include <boost/asio/basic_socket.hpp>
#include <boost/asio/ip/unicast.hpp>
#include "../../contrib/epee/include/net/abstract_tcp_server2.h"

#include "../../contrib/otshell_utils/utils.hpp"
#include "data_logger.hpp"
using namespace nOT::nUtils;

// TODO:
#include "../../src/p2p/network_throttle-detail.hpp"
#include "../../src/adolfcoin_core/adolfcoin_core.h"

// ################################################################################################
// local (TU local) headers
// ################################################################################################

namespace epee
{
namespace net_utils
{

  std::string to_string(t_connection_type type)
  {
	  if (type == e_connection_type_NET)
		return std::string("NET");
	  else if (type == e_connection_type_RPC)
	    return std::string("RPC");
	  else if (type == e_connection_type_P2P)
	    return std::string("P2P");
	  
	  return std::string("UNKNOWN");
  }


/* ============================================================================ */

class connection_basic_pimpl {
	public:
		connection_basic_pimpl(const std::string &name);

		static int m_default_tos;

		network_throttle_bw m_throttle; // per-perr
    critical_section m_throttle_lock;

		int m_peer_number; // e.g. for debug/stats
};


} // namespace
} // namespace

// ################################################################################################
// The implementation part
// ################################################################################################

namespace epee
{
namespace net_utils
{

// ================================================================================================
// connection_basic_pimpl
// ================================================================================================
	
connection_basic_pimpl::connection_basic_pimpl(const std::string &name) : m_throttle(name) { }

// ================================================================================================
// connection_basic
// ================================================================================================

// static variables:
int connection_basic_pimpl::m_default_tos;

// methods:
connection_basic::connection_basic(boost::asio::io_service& io_service, std::atomic<long> &ref_sock_count, std::atomic<long> &sock_number)
	: 
	mI( new connection_basic_pimpl("peer") ),
	strand_(io_service),
	socket_(io_service),
	m_want_close_connection(false), 
	m_was_shutdown(false),
	m_ref_sock_count(ref_sock_count)
{ 
	++ref_sock_count; // increase the global counter
	mI->m_peer_number = sock_number.fetch_add(1); // use, and increase the generated number

	string remote_addr_str = "?";
	try { remote_addr_str = socket_.remote_endpoint().address().to_string(); } catch(...){} ;

	_note("Spawned connection p2p#"<<mI->m_peer_number<<" to " << remote_addr_str << " currently we have sockets count:" << m_ref_sock_count);
	
}

connection_basic::~connection_basic() {
	string remote_addr_str = "?";
	try { remote_addr_str = socket_.remote_endpoint().address().to_string(); } catch(...){} ;
	_note("Destructing connection p2p#"<<mI->m_peer_number << " to " << remote_addr_str);
}

void connection_basic::set_rate_up_limit(uint64_t limit) {
	
	// TODO remove __SCALING_FACTOR...
	const double SCALING_FACTOR = 2.1; // to acheve the best performance
	limit *= SCALING_FACTOR;
	{
		CRITICAL_REGION_LOCAL(	network_throttle_manager::m_lock_get_global_throttle_out );
		network_throttle_manager::get_global_throttle_out().set_target_speed(limit);
		network_throttle_manager::get_global_throttle_out().set_real_target_speed(limit / SCALING_FACTOR);
	}
	save_limit_to_file(limit);
}

void connection_basic::set_rate_down_limit(uint64_t limit) {
	{
	  CRITICAL_REGION_LOCAL(	network_throttle_manager::m_lock_get_global_throttle_in );
		network_throttle_manager::get_global_throttle_in().set_target_speed(limit);
	}

	{
	  CRITICAL_REGION_LOCAL(	network_throttle_manager::m_lock_get_global_throttle_inreq );
		network_throttle_manager::get_global_throttle_inreq().set_target_speed(limit);
	}
    save_limit_to_file(limit);
}

uint64_t connection_basic::get_rate_up_limit() {
    uint64_t limit;
    {
         CRITICAL_REGION_LOCAL( network_throttle_manager::m_lock_get_global_throttle_out );
         limit = network_throttle_manager::get_global_throttle_out().get_target_speed();
	}
    return limit;
}

uint64_t connection_basic::get_rate_down_limit() {
    uint64_t limit;
    {
         CRITICAL_REGION_LOCAL( network_throttle_manager::m_lock_get_global_throttle_in );
         limit = network_throttle_manager::get_global_throttle_in().get_target_speed();
	}
    return limit;
}

void connection_basic::save_limit_to_file(int limit) {
    // saving limit to file
    if (!epee::net_utils::data_logger::m_save_graph)
		return;

    {
         CRITICAL_REGION_LOCAL(        network_throttle_manager::m_lock_get_global_throttle_out );
               epee::net_utils::data_logger::get_instance().add_data("upload_limit", network_throttle_manager::get_global_throttle_out().get_target_speed() / 1024);
	}
	
    {
         CRITICAL_REGION_LOCAL(        network_throttle_manager::m_lock_get_global_throttle_in );
               epee::net_utils::data_logger::get_instance().add_data("download_limit", network_throttle_manager::get_global_throttle_in().get_target_speed() / 1024);
	}
}
 
void connection_basic::set_tos_flag(int tos) {
	connection_basic_pimpl::m_default_tos = tos;
}

int connection_basic::get_tos_flag() {
	return connection_basic_pimpl::m_default_tos;
}

void connection_basic::sleep_before_packet(size_t packet_size, int phase,  int q_len) {
	double delay=0; // will be calculated
	do
	{ // rate limiting
		if (m_was_shutdown) { 
			_dbg2("m_was_shutdown - so abort sleep");
			return;
		}

		{ 
			CRITICAL_REGION_LOCAL(	network_throttle_manager::m_lock_get_global_throttle_out );
			delay = network_throttle_manager::get_global_throttle_out().get_sleep_time_after_tick( packet_size ); // decission from global
		}

		delay *= 0.50;
		if (delay > 0) {
            long int ms = (long int)(delay * 1000);
			_info_c("net/sleep", "Sleeping in " << __FUNCTION__ << " for " << ms << " ms before packet_size="<<packet_size); // debug sleep
			_dbg1("sleep in sleep_before_packet");
			epee::net_utils::data_logger::get_instance().add_data("sleep_up", ms);
			boost::this_thread::sleep(boost::posix_time::milliseconds( ms ) );
		}
	} while(delay > 0);

// XXX LATER XXX
	{
	  CRITICAL_REGION_LOCAL(	network_throttle_manager::m_lock_get_global_throttle_out );
		network_throttle_manager::get_global_throttle_out().handle_trafic_exact( packet_size * 700); // increase counter - global
	}

}
void connection_basic::set_start_time() {
	CRITICAL_REGION_LOCAL(	network_throttle_manager::m_lock_get_global_throttle_out );
	m_start_time = network_throttle_manager::get_global_throttle_out().get_time_seconds();
}

void connection_basic::do_send_handler_write(const void* ptr , size_t cb ) {
	sleep_before_packet(cb,1,-1);
	_info_c("net/out/size", "handler_write (direct) - before ASIO write, for packet="<<cb<<" B (after sleep)");
	set_start_time();
}

void connection_basic::do_send_handler_write_from_queue( const boost::system::error_code& e, size_t cb, int q_len ) {
	sleep_before_packet(cb,2,q_len);
	_info_c("net/out/size", "handler_write (after write, from queue="<<q_len<<") - before ASIO write, for packet="<<cb<<" B (after sleep)");

	set_start_time();
}

void connection_basic::logger_handle_net_read(size_t size) { // network data read
    size /= 1024;
    epee::net_utils::data_logger::get_instance().add_data("download", size);
}

void connection_basic::logger_handle_net_write(size_t size) {
    size /= 1024;
    epee::net_utils::data_logger::get_instance().add_data("upload", size);	
}

double connection_basic::get_sleep_time(size_t cb) {
	CRITICAL_REGION_LOCAL(epee::net_utils::network_throttle_manager::network_throttle_manager::m_lock_get_global_throttle_out);
    auto t = network_throttle_manager::get_global_throttle_out().get_sleep_time(cb);
    return t;
}

void connection_basic::set_save_graph(bool save_graph) {
	epee::net_utils::data_logger::m_save_graph = save_graph;
}


} // namespace
} // namespace

