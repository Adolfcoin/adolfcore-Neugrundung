################################################################################
# 	This file is part of the Adolfcoin project and is Copyright © The		   #
# 	Contributors listed in the AUTHORS file at the project root. This project  #
# 	is released under the MPL v2 license contained in the LICENSE file at the  #
# 	root of this project.                                                      #
#                                                                              #
# 	Any derived works must be released under the MPL v2 (or later) license.    #
# 																			   #
# 	Parts of this project are originally Copyright © The Monero Project, The   #
# 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash  #
# 	Developers. For full details see the LEGACY_LICENSES file at the root of   #
# 	this project.                                                              #
################################################################################
cmake_minimum_required (VERSION 2.6)
project (adolfcoin CXX)

file(GLOB P2P *)
source_group(p2p FILES ${P2P})

#add_library(p2p ${P2P})

#adolfs_private_headers(p2p ${P2P})
adolfcoin_add_library(p2p ${P2P})
#target_link_libraries(p2p)
#  LINK_PRIVATE
#    ${Boost_CHRONO_LIBRARY}
#    ${Boost_REGEX_LIBRARY}
#    ${Boost_SYSTEM_LIBRARY}
#    ${Boost_THREAD_LIBRARY}
#    ${EXTRA_LIBRARIES})
add_dependencies(p2p
  version)
