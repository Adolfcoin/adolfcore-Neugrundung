/**
@file
@author rfree  
@brief interface for throttling of connection (count and rate-limit speed etc) 
@details <PRE>

Throttling work by:
1) taking note of all traffic (hooks added e.g. to connection class) and measuring speed
2) depending on that information we sleep before sending out data (or send smaller portions of data)
3) depending on the information we can also sleep before sending requests or ask for smaller sets of data to download

</PRE> 

@image html images/net/rate1-down-1k.png
@image html images/net/rate1-down-full.png
@image html images/net/rate1-up-10k.png
@image html images/net/rate1-up-full.png
@image html images/net/rate2-down-100k.png
@image html images/net/rate2-down-10k.png
@image html images/net/rate2-down-50k.png
@image html images/net/rate2-down-full.png
@image html images/net/rate2-up-100k.png
@image html images/net/rate2-up-10k.png
@image html images/net/rate3-up-10k.png


*/

////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////
// 

#include "../../src/p2p/network_throttle-detail.hpp"

namespace epee
{
namespace net_utils
{

// ================================================================================================
// network_throttle_manager
// ================================================================================================

// ================================================================================================
// static:
std::mutex network_throttle_manager::m_lock_get_global_throttle_in;
std::mutex network_throttle_manager::m_lock_get_global_throttle_inreq;
std::mutex network_throttle_manager::m_lock_get_global_throttle_out;

int network_throttle_manager::xxx;


// ================================================================================================
// methods:
i_network_throttle & network_throttle_manager::get_global_throttle_in() { 
	std::call_once(m_once_get_global_throttle_in, [] { m_obj_get_global_throttle_in.reset(new network_throttle("in/all","<<< global-IN",10)); }	);
	return * m_obj_get_global_throttle_in;
}
std::once_flag network_throttle_manager::m_once_get_global_throttle_in;
std::unique_ptr<i_network_throttle> network_throttle_manager::m_obj_get_global_throttle_in;



i_network_throttle & network_throttle_manager::get_global_throttle_inreq() { 
	std::call_once(m_once_get_global_throttle_inreq, [] { m_obj_get_global_throttle_inreq.reset(new network_throttle("inreq/all", "<== global-IN-REQ",10)); }	);
	return * m_obj_get_global_throttle_inreq;
}
std::once_flag network_throttle_manager::m_once_get_global_throttle_inreq;
std::unique_ptr<i_network_throttle> network_throttle_manager::m_obj_get_global_throttle_inreq;


i_network_throttle & network_throttle_manager::get_global_throttle_out() { 
	std::call_once(m_once_get_global_throttle_out, [] { m_obj_get_global_throttle_out.reset(new network_throttle("out/all", ">>> global-OUT",10)); }	);
	return * m_obj_get_global_throttle_out;
}
std::once_flag network_throttle_manager::m_once_get_global_throttle_out;
std::unique_ptr<i_network_throttle> network_throttle_manager::m_obj_get_global_throttle_out;




network_throttle_bw::network_throttle_bw(const std::string &name1) 
	: m_in("in/"+name1, name1+"-DOWNLOAD"), m_inreq("inreq/"+name1, name1+"-DOWNLOAD-REQUESTS"), m_out("out/"+name1, name1+"-UPLOAD")
{ }




} // namespace 
} // namespace 





