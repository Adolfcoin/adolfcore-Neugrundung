////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma  once 


#define CORE_RPC_ERROR_CODE_WRONG_PARAM           -1
#define CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT        -2
#define CORE_RPC_ERROR_CODE_TOO_BIG_RESERVE_SIZE  -3
#define CORE_RPC_ERROR_CODE_WRONG_WALLET_ADDRESS  -4
#define CORE_RPC_ERROR_CODE_INTERNAL_ERROR        -5
#define CORE_RPC_ERROR_CODE_WRONG_BLOCKBLOB       -6
#define CORE_RPC_ERROR_CODE_BLOCK_NOT_ACCEPTED    -7
#define CORE_RPC_ERROR_CODE_CORE_BUSY             -9
#define CORE_RPC_ERROR_CODE_WRONG_BLOCKBLOB_SIZE  -10


