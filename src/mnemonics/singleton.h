////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

/*!
 * \file singleton.h
 * 
 * \brief A singleton helper class based on template.
 */

/*!
 * \namespace Language
 * \brief Mnemonic language related namespace.
 */
namespace Language
{
  /*!
   * \class Singleton
   * 
   * \brief Single helper class.
   * 
   * Do Language::Singleton<YourClass>::instance() to create a singleton instance
   * of `YourClass`.
   */
  template <class T>
  class Singleton
  {
    Singleton() {}
    Singleton(Singleton &s) {}
    Singleton& operator=(const Singleton&) {}
  public:
    static T* instance()
    {
      static T* obj = new T;
      return obj;
    }
  };
}
