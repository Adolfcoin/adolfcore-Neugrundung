////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#include "checkpoints_create.h"
#include "common/dns_utils.h"
#include "include_base_utils.h"
#include <sstream>
#include <random>
#include "storages/portable_storage_template_helper.h" // epee json include

namespace
{
  bool dns_records_match(const std::vector<std::string>& a, const std::vector<std::string>& b)
  {
    if (a.size() != b.size()) return false;

    for (const auto& record_in_a : a)
    {
      bool ok = false;
      for (const auto& record_in_b : b)
      {
	if (record_in_a == record_in_b)
	{
	  ok = true;
	  break;
	}
      }
      if (!ok) return false;
    }

    return true;
  }
} // anonymous namespace

namespace cryptonote
{

struct t_hashline 
{
	uint64_t height;
	std::string hash;
      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(height)
        KV_SERIALIZE(hash)
      END_KV_SERIALIZE_MAP()
};

struct t_hash_json {
 	std::vector<t_hashline> hashlines;
      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(hashlines)
      END_KV_SERIALIZE_MAP()
};

bool create_checkpoints(cryptonote::checkpoints& checkpoints)
{      
  //ADD_CHECKPOINT(1,     "771fbcd656ec1464d3a02ead5e18644030007a0fc664c0a964d30922821a8148");

  return true;
}

bool load_checkpoints_from_json(cryptonote::checkpoints& checkpoints, std::string json_hashfile_fullpath)
{
  boost::system::error_code errcode;
  if (! (boost::filesystem::exists(json_hashfile_fullpath, errcode)))
  {
    LOG_PRINT_L1("Blockchain checkpoints file not found");
    return true;
  }

  LOG_PRINT_L1("Adding checkpoints from blockchain hashfile");

  uint64_t prev_max_height = checkpoints.get_max_height();
  LOG_PRINT_L1("Hard-coded max checkpoint height is " << prev_max_height);
  t_hash_json hashes;
  epee::serialization::load_t_from_json_file(hashes, json_hashfile_fullpath);
  for (std::vector<t_hashline>::const_iterator it = hashes.hashlines.begin(); it != hashes.hashlines.end(); )
  {
      uint64_t height;
      height = it->height;
      if (height <= prev_max_height) {
	LOG_PRINT_L1("ignoring checkpoint height " << height);
      } else {
	std::string blockhash = it->hash;
	LOG_PRINT_L1("Adding checkpoint height " << height << ", hash=" << blockhash);
	ADD_CHECKPOINT(height, blockhash);
      }
      ++it;
  }

  return true;
}

bool load_checkpoints_from_dns(cryptonote::checkpoints& checkpoints, bool testnet)
{
  // All SS checkpoint domains have DNSSEC on and valid
  static const std::vector<std::string> dns_urls = { "checkpoints.adolfcoin.camp"
  };
  static const std::vector<std::string> testnet_dns_urls = { "testpoints.adolfcoin.camp"
  };

  std::vector<std::vector<std::string> > records;
  records.resize(dns_urls.size());

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<int> dis(0, dns_urls.size() - 1);
  size_t first_index = dis(gen);

  bool avail, valid;
  size_t cur_index = first_index;
  do
  {
    std::string url;
    if (testnet)
    {
      url = testnet_dns_urls[cur_index];
    }
    else
    {
      url = dns_urls[cur_index];
    }

    records[cur_index] = tools::DNSResolver::instance().get_txt_record(url, avail, valid);
    if (!avail)
    {
      LOG_PRINT_L2("DNSSEC not available for checkpoint update at URL: " << url << ", skipping.");
    }
    if (!valid)
    {
      LOG_PRINT_L2("DNSSEC validation failed for checkpoint update at URL: " << url << ", skipping.");
    }

    if (records[cur_index].size() == 0 || !avail || !valid)
    {
      cur_index++;
      if (cur_index == dns_urls.size())
      {
	cur_index = 0;
      }
      records[cur_index].clear();
      continue;
    }
    break;
  } while (cur_index != first_index);

  size_t num_valid_records = 0;

  for( const auto& record_set : records)
  {
    if (record_set.size() != 0)
    {
      num_valid_records++;
    }
  }

  if (num_valid_records < 2)
  {
    //LOG_PRINT_L0("Not currently using SS checkpoints"); removed till checkpoints deployed
    return true;
  }

  int good_records_index = -1;
  for (size_t i = 0; i < records.size() - 1; ++i)
  {
    if (records[i].size() == 0) continue;

    for (size_t j = i + 1; j < records.size(); ++j)
    {
      if (dns_records_match(records[i], records[j]))
      {
	good_records_index = i;
	break;
      }
    }
    if (good_records_index >= 0) break;
  }

  if (good_records_index < 0)
  {
    LOG_PRINT_L0("WARNING: no two SS checkpoint records matched");
    return true;
  }

  for (auto& record : records[good_records_index])
  {
    auto pos = record.find(":");
    if (pos != std::string::npos)
    {
      uint64_t height;
      crypto::hash hash;

      // parse the first part as uint64_t,
      // if this fails move on to the next record
      std::stringstream ss(record.substr(0, pos));
      if (!(ss >> height))
      {
	continue;
      }

      // parse the second part as crypto::hash,
      // if this fails move on to the next record
      std::string hashStr = record.substr(pos + 1);
      if (!epee::string_tools::parse_tpod_from_hex_string(hashStr, hash))
      {
	continue;
      }

      ADD_CHECKPOINT(height, hashStr);
    }
  }
  return true;
}

bool load_new_checkpoints(cryptonote::checkpoints& checkpoints, std::string json_hashfile_fullpath)
{
  // TODO: replace hard-coded url with const string or #define
  return (load_checkpoints_from_json(checkpoints, json_hashfile_fullpath) && load_checkpoints_from_dns(checkpoints));
}

}  // namespace cryptonote
