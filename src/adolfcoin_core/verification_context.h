////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once
namespace cryptonote
{
  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct tx_verification_context
  {
    bool m_should_be_relayed;
    bool m_verifivation_failed; //bad tx, should drop connection
    bool m_verifivation_impossible; //the transaction is related with an alternative blockchain
    bool m_added_to_pool; 
  };

  struct block_verification_context
  {
    bool m_added_to_main_chain;
    bool m_verifivation_failed; //bad block, should drop connection
    bool m_marked_as_orphaned;
    bool m_already_exists;
  };
}
