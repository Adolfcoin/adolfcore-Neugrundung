////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <boost/uuid/uuid.hpp>

#define CRYPTONOTE_DNS_TIMEOUT_MS                       20000

#define CRYPTONOTE_MAX_BLOCK_NUMBER                     500000000
#define CRYPTONOTE_MAX_BLOCK_SIZE                       500000000  // block header blob limit, never used!
#define CRYPTONOTE_GETBLOCKTEMPLATE_MAX_BLOCK_SIZE		196608 //size of block (bytes) that is the maximum that miners will produce
#define CRYPTONOTE_MAX_TX_SIZE                          1000000000
#define CRYPTONOTE_PUBLIC_ADDRESS_TEXTBLOB_VER          0
#define CRYPTONOTE_MINED_MONEY_UNLOCK_WINDOW            120
#define CURRENT_TRANSACTION_VERSION                     1
#define CURRENT_BLOCK_MAJOR_VERSION                     1
#define CURRENT_BLOCK_MINOR_VERSION                     0

/* How far in the future (in seconds) can a block be created and accepted by the network.
 * All nodes on the network should use NTP to ensure their clocks are accurate. */
#define CRYPTONOTE_BLOCK_FUTURE_TIME_LIMIT              60*60*1

/* How far in the past can a timestamp be before a block is rejected.
 * The last x blocks here are taken, their timestamps sorted, and the
 * median timestamp compared to that of the current block being validated.
 * If the timestamp of the current block is less than the median timestamp
 * then the it will be gassed. */
#define BLOCKCHAIN_TIMESTAMP_CHECK_WINDOW               30

/* MONEY_SUPPLY is the total number coins that will ever be produced. */
#define MONEY_SUPPLY                                    ((uint64_t)(-327622852568399797))//18119121221141151819

/* Where should the decimal point appear in the above number? */
#define CRYPTONOTE_DISPLAY_DECIMAL_POINT                11 //181,191,212.21141151819 coins in total

/*Total coins that existed before the Neugrundung which need to be provided to miners*/
#define NEUGRUNDUNG_COUNT                               (29000000000000000) //290,000.00000000000

/*Used in emission calcuation to determine how long it will take for all coins to be mined. */
#define EMISSION_RATIO                                  47451488 //1000 years of coins for a 1000 year Reich.

//#define FINAL_SUBSIDY_PER_MINUTE                        ((uint64_t)3000000000) // 3 * pow(10, 11)

/* Number of blocks before the next release/fork (when this client dies) */
#define GAS_HEIGHT                                      121488

#define CRYPTONOTE_REWARD_BLOCKS_WINDOW                 100
#define CRYPTONOTE_BLOCK_GRANTED_FULL_REWARD_ZONE       60000 //size of block (bytes) after which reward for block calculated using block size
#define CRYPTONOTE_COINBASE_BLOB_RESERVED_SIZE          600

// COIN - number of smallest units in one coin
#define COIN                                            ((uint64_t)100000000000) // pow(10, 11)

#define FEE_PER_KB                                      ((uint64_t)1000000000) // pow(10, 9) 2000000000 10000000000

#define ORPHANED_BLOCKS_MAX_COUNT                       100


#define DIFFICULTY_TARGET                               120  // seconds

/* DIFFICULTY_WINDOW: the number of previous blocks used by the algorithm
   to estimate the hash rate. The algorithm will adjust to small changes
   in the hash rate in approximately DIFFICULTY_TARGET*DIFFICULTY_WINDOW (seconds). */
#define DIFFICULTY_WINDOW                               180 //Approximately 6 hours of blocks

/* the number of last blocks that should be discarded prior
   to any difficulty calculation. This is done to make it harder to
   create a fork with higher cumulative difficulty. */
#define DIFFICULTY_LAG                                  10

/* DIFFICULTY_CUT: the number of highest and lowest timestamp values to be
   ignored, as they are considered to be outliers. */
#define DIFFICULTY_CUT                                  16

#define DIFFICULTY_BLOCKS_COUNT                         DIFFICULTY_WINDOW + DIFFICULTY_LAG


#define CRYPTONOTE_LOCKED_TX_ALLOWED_DELTA_SECONDS      DIFFICULTY_TARGET * CRYPTONOTE_LOCKED_TX_ALLOWED_DELTA_BLOCKS
#define CRYPTONOTE_LOCKED_TX_ALLOWED_DELTA_BLOCKS       1


#define BLOCKS_IDS_SYNCHRONIZING_DEFAULT_COUNT          20000  //by default, blocks ids count in synchronizing
#define BLOCKS_SYNCHRONIZING_DEFAULT_COUNT              176    //this sets the number of blocks that the daemon will request from peers unless there's a reason to request less.
#define CRYPTONOTE_PROTOCOL_HOP_RELAX_COUNT             5      //value of hop, after which we use only announce new blocks but don't send them

#define CRYPTONOTE_MEMPOOL_TX_LIVETIME                    21600 //discard transaction if older than 6 hours
#define CRYPTONOTE_MEMPOOL_TX_FROM_ALT_BLOCK_LIVETIME     345600 //discard transaction if older than 4 days

#define COMMAND_RPC_GET_BLOCKS_FAST_MAX_COUNT           1000

#define P2P_LOCAL_WHITE_PEERLIST_LIMIT                  1000
#define P2P_LOCAL_GRAY_PEERLIST_LIMIT                   5000

#define P2P_DEFAULT_CONNECTIONS_COUNT                   18
#define P2P_DEFAULT_HANDSHAKE_INTERVAL                  60           //seconds
#define P2P_DEFAULT_PACKET_MAX_SIZE                     50000000     //50000000 bytes maximum packet size
#define P2P_DEFAULT_PEERS_IN_HANDSHAKE                  250
#define P2P_DEFAULT_CONNECTION_TIMEOUT                  5000       //5 seconds
#define P2P_DEFAULT_PING_CONNECTION_TIMEOUT             2000       //2 seconds
#define P2P_DEFAULT_INVOKE_TIMEOUT                      60*2*1000  //2 minutes
#define P2P_DEFAULT_HANDSHAKE_INVOKE_TIMEOUT            5000       //5 seconds
#define P2P_DEFAULT_WHITELIST_CONNECTIONS_PERCENT       60

#define ALLOW_DEBUG_COMMANDS

#define SHORT_REICHNAME                         "adolfcoin"
#define FORK_DATA_FOLDER                        "neugrundung"
#define CRYPTONOTE_POOLDATA_FILENAME            "poolstate.bin"
#define CRYPTONOTE_BLOCKCHAINDATA_FILENAME      "blocktrain.bin"
#define CRYPTONOTE_BLOCKCHAINDATA_TEMP_FILENAME "blocktrain.bin.tmp"
#define P2P_NET_DATA_FILENAME                   "p2pstate.bin"
#define MINER_CONFIG_FILE_NAME                  "meiner_conf.json" 

#define THREAD_STACK_SIZE                       5 * 1024 * 1024

#define GENESIS_COINBASE_TX_HEX                 "017801ff00018080f2d38aebc1330204f3ab5bf9c19f9304f143809ae932ac333f63476d233a8c6a4ace199e9fc9fe2101d009cdbf41313c486f463af29e8e836b531651459b69336d688bf8885fcecda0"

namespace config
{
  uint64_t const DEFAULT_FEE_ATOMIC_PER_KB = 500; // Rather Experimental
  uint8_t const FEE_CALCULATION_MAX_RETRIES = 10;
  uint64_t const DEFAULT_DUST_THRESHOLD = ((uint64_t)2000000000); // pow(10, 10)
  std::string const P2P_REMOTE_DEBUG_TRUSTED_PUB_KEY = "0000000000000000000000000000000000000000000000000000000000000000";

  uint64_t const CRYPTONOTE_PUBLIC_ADDRESS_BASE58_PREFIX = 90;
  uint16_t const P2P_DEFAULT_PORT = 48688;
  uint16_t const RPC_DEFAULT_PORT = 1488;
  boost::uuids::uuid const NETWORK_ID = { {
      0x67, 0x6f, 0x79, 0x63, 0x6f,  0x69, 0x6E, 0x69, 0x73, 0x63, 0x6F, 0x6D, 0x69, 0x6E, 0x67
    } };
  uint32_t const GENESIS_NONCE = 18890420;

  namespace testnet 
  {
    uint64_t const CRYPTONOTE_PUBLIC_ADDRESS_BASE58_PREFIX = 0x1b2dec; 
    uint16_t const P2P_DEFAULT_PORT = 12362;
    uint16_t const RPC_DEFAULT_PORT = 10416;
    boost::uuids::uuid const NETWORK_ID = { {
        0x6b, 0x65, 0x6b, 0x69, 0x73, 0x74, 0x61, 0x6E, 0x66, 0x72, 0x65, 0x65, 0x64, 0x6F, 0x6D, 0x2E
      } }; 
    uint32_t const GENESIS_NONCE = 42089;
    
    
  }
}
