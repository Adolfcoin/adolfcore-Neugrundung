////////////////////////////////////////////////////////////////////////////////
// 	This file is part of the Adolfcoin project and is Copyright © The		  //
// 	Contributors listed in the AUTHORS file at the project root. This project //
// 	is released under the MPL v2 license contained in the LICENSE file at the //
// 	root of this project.                                                     //
//                                                                            //
// 	Any derived works must be released under the MPL v2 (or later) license.   //
// 																			  //
// 	Parts of this project are originally Copyright © The Monero Project, The  //
// 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash //
// 	Developers. For full details see the LEGACY_LICENSES file at the root of  //
// 	this project.                                                             //
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <map>
#include <vector>
#include "adolfcoin_helpers.h"


namespace cryptonote
{
  class checkpoints
  {
  public:
    checkpoints();
    bool add_checkpoint(uint64_t height, const std::string& hash_str);
    bool is_in_checkpoint_zone(uint64_t height) const;
    bool check_block(uint64_t height, const crypto::hash& h) const;
    bool check_block(uint64_t height, const crypto::hash& h, bool& is_a_checkpoint) const;
    bool is_alternative_block_allowed(uint64_t blockchain_height, uint64_t block_height) const;
    uint64_t get_max_height() const;
    const std::map<uint64_t, crypto::hash>& get_points() const;
    bool check_for_conflicts(const checkpoints& other) const;
  private:
    std::map<uint64_t, crypto::hash> m_points;
  };
}
