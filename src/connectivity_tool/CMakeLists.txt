################################################################################
# 	This file is part of the Adolfcoin project and is Copyright © The		   #
# 	Contributors listed in the AUTHORS file at the project root. This project  #
# 	is released under the MPL v2 license contained in the LICENSE file at the  #
# 	root of this project.                                                      #
#                                                                              #
# 	Any derived works must be released under the MPL v2 (or later) license.    #
# 																			   #
# 	Parts of this project are originally Copyright © The Monero Project, The   #
# 	Cryptonote Developers, Bitcoin Developers, The Zcash developers, and Dash  #
# 	Developers. For full details see the LEGACY_LICENSES file at the root of   #
# 	this project.                                                              #
################################################################################

set(connectivity_tool_sources
  conn_tool.cpp)

set(connectivity_tool_private_headers)

adolfcoin_add_executable(connectivity_tool
  ${connectivity_tool_sources}
  ${connectivity_tool_private_headers})
target_link_libraries(connectivity_tool
  LINK_PRIVATE
    adolfcoin_core
    crypto
    common
    ${CMAKE_THREAD_LIBS_INIT}
    ${Boost_PROGRAM_OPTIONS_LIBRARY}
    ${Boost_REGEX_LIBRARY}
    ${Boost_SYSTEM_LIBRARY})
